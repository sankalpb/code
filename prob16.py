#!/usr/bin/python2

import sys
f = sys.stdin.read().split('\n')
t = int(f[0])

def bfs2(u, distance):
    if distance[u][0] == 1:
        return
    if len(ad_list[u]) == 1:
        edge = ad_list[u][0]
        if distance[edge[0]][0] != 1:
            bfs2(edge[0], distance)
        distance[u] = [ edge[1] + d for d in distance[edge[0]] ]
        distance[u][0] = 1
        return
    distance[u] = [0 for j in range(N+1)]
    visited = [0]*(N+1)
    cost = 0
    visited[u] = 1
    q = [(u, cost)]
    while q:
        vertex,cost = q.pop(0)
        for edge in ad_list[vertex]:
            ux = edge[0]
            if visited[ux] == 0:
                distance[u][ux] = edge[1] + cost
                q.append((ux, edge[1] + cost))
                visited[ux] = 1
    distance[u][0] = 1
    

def do_msp(ad_list, edges):
    forest = [[i] for i in range(N + 1)]
    for edge in sorted(edges, key = lambda x: x[2]):
        u = edge[0]
        v = edge[1]
        if len(forest[v]) < len(forest[u]):
            tmp = u
            u = v
            v = tmp
        if v in forest[u]:
            pass
        else:
            ad_list[u].append((v, edge[2]))
            ad_list[v].append((u, edge[2]))
            forest[v].extend(forest[u])
            if len(forest[v]) == N:
                return
            for node in forest[u]:
                forest[node] = forest[v]


            
def print_buf():
    print('\n'.join(map(str, buf)))


i = 1
for case in range(t):
    line2 = f[i].split()
    i += 1
    N = int(line2[0])
    P = int(line2[1])
    M = int(line2[2])
    
    ad_list = [[] for ii in range(N + 1)]
    edges = []
    print 'Case:', case + 1
    for n_edge in range(P):
        line = f[i].split()
        i += 1
        edge = (int(line[0]), int(line[1]), int(line[2]))
        edges.append(edge)
    do_msp(ad_list, edges)

    distance = [[0] for ii in range(N+1)]

    for node in range(N):
        bfs2(node + 1, distance)
        
    buf = ''
    for q in range(M):
        line = f[i].split()
        i += 1
        (u, v) = (int(line[0]), int(line[1]))
        buf.join([str(distance[u][v])])
        #min_length(u, v, distance)
    print_buf()
    buf = ''
        
