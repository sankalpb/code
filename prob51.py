from typing import List

class Solution:
    result = []
    def lis(self, nums, j):
        if j < 2:
            if nums[-1] > nums[-2]:
                self.result = [(nums[-2], 2), (nums[-1], 1)]
            else:
                self.result = [(max(nums[-2:-1]), 1)]
        else:
            max_len = 0
            max_list = (nums[len(nums) - 1 - j], 1)
            #print(result)

            for li in self.result:
                #print(li[0], nums[len(nums) - 1 - j])
                if nums[len(nums) - 1 - j] < li[0] and (li[1] + 1) > max_len:
                    max_len = li[1] + 1
                    max_list = (nums[len(nums) - 1 - j], max_len)
            self.result.append(max_list)
        
    def lengthOfLIS(self, nums: List[int]) -> int:
        if len(nums) < 2:
            return len(nums)
        for j in range(len(nums)):
            #print(j)
            self.lis(nums, j)
        return max([li[1] for li in self.result])

s = Solution()

print(s.lengthOfLIS(list(range(1, 2500))))
