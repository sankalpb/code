import sys
from bitarray import bitarray


class Board:
    def __init__(self, p):
        self.complete = 0
        self.matrix = []
        self.empty = 0
        for i in range(size):
            self.matrix.append([])
            for j in range(size):
                if int(p[9*i+j]) == 0:
                       self.empty += 1
                self.matrix[i].append(int(p[9*i+j]))
    def __repr__(self):
        s = ''
        for i in range(size):
            for j in range(size):
                s = s + ' ' + str(self.matrix[i][j]) + ' '
            s = s + '\n'
        return s
    def checksolution(self, trace=0):
        if self.emptycount():
            return False
        vec = bitarray(81*3)
        vec.setall(False)
        for i in range(size):
            for j in range(size):
                if self.matrix[i][j] != 0:
                    vec[i*9 + self.matrix[i][j] - 1] = True
                    vec[81 - 1 + j * 9 + self.matrix[i][j]] = True
                    vec[(81)*2 - 1 + (int(i/3)*3 + int(j/3))*9 + self.matrix[i][j]] = True
        if trace:
            print(vec[0:81])
            print(vec[81:162])
            print(vec[162:243])
        return(vec.all())
    def candidate(self, i, j, c, trace=0):
        vec = bitarray(9)
        vec.setall(False)
        for k in range(size):
            if self.matrix[k][j] != 0:
                vec[self.matrix[k][j] - 1] = True
            if self.matrix[i][k] != 0:
                vec[self.matrix[i][k] - 1] = True
            if self.matrix[int(i/3)*3 + int(k/3)][int(j/3)*3 + k%3] != 0:
                vec[self.matrix[int(i/3)*3 + int(k/3)][int(j/3)*3 + k%3] - 1] = True
        c.extend([i+1 for (i, bit) in enumerate(vec) if bit == 0])
        if trace:
            print(vec)
        return c
    def findbestcell(self, cell, trace=0):
        min = 10
        for i in range(size):
            for j in range(size):
                if self.matrix[i][j] != 0:
                    continue
                c = []
                self.candidate(i, j, c, trace)
                if trace:
                    print(i, j, c)
                if c != []:
                    if len(c) < min:
                        min = len(c)
                        cell[0] = i
                        cell[1] = j
                        cell[2] = c
    def emptycount(self):
        return self.empty
    def backtrack(self):
        if not self.complete:
            if self.checksolution():
                self.complete = 1
                print('Solved:')
                print(self)
                return
            cell = [0, 0, []]
            self.findbestcell(cell)
            #print(cell)
            i = cell[0]
            j = cell[1]
            for candidate in cell[2]:
                self.matrix[i][j] = candidate
                self.empty -= 1
                self.backtrack()
                self.matrix[i][j] = 0
                self.empty += 1


size = 9
problem = '003020600900305001001806400008102900700000008006708200002609500800203009005010300'
problem2 = '200080300060070084030500209000105408000000000402706000301007040720040060004010003'
b2 = Board(problem2)
print(b2)
b2.backtrack()
