import random
import sys

n = int(sys.argv[1])



def medianofmedians(l):
    i = 5
    b = []
    while (i <= len(l)):
        x = sorted(l[i - 5: i])[2]
        b.append(x)
        i = i + 5
    if (i - 5 < len(l)):
        b.extend(l[i - 5: ])
    if len(b) <= 5:
        return sorted(b)[int(len(b)/2)]
    else:
        return medianofmedians(b)


def pivot(c, p):
    index = c.index(p)
    start = 0
    i = 0
    end = len(c) - 1
    temp = c[index]
    c[index] = c[end]
    c[end] = temp
    
    while i < len(c) - 1:
        print(c)
        if c[i] < p or (c[i] == p and random.randint(0,1) == 0):
            temp = c[start]
            c[start] = c[i]
            c[i] = temp
            start = start + 1
        i = i + 1 
    temp = c[start]
    c[start] = c[end]
    c[end] = temp
    return (c, start)


def findkth(d, k):
    print(len(d))
    p = medianofmedians(d)
    print(p)
    (d, index) = pivot(d, p)
    if index == k:
        return d[index]
    elif index < k:
        return findkth(d[index + 1: ], k - index - 1)
    elif index > k:
        return findkth(d[: index], k)
    

random.seed()
a = []
for i in range(n):
    if i > 2 and int(i * 100 / n ) % 10 == 0 and int((i - 1)* 100 / n) % 10 != 0:
        print(int(i * 100/ n ))
    a.append(random.randint(0,512))
length = len(a)
if len(a) % 2 == 0:
    print(sorted(a)[int(length/2) - 1], sorted(a)[int(length/2)]) 
else:
    print(sorted(a)[int(length/2)])

print(findkth(a, int(length/2)))


