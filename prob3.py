import sys
count = int(sys.stdin.readline())

for line in sys.stdin:	
    bt1, bt2, mt1, mt2 = map(int, line.split())
    if bt1 > bt2:
        temp = bt1
        bt1 = bt2
        bt2 = temp
    if mt1 > mt2:
        temp = mt1
        mt1 = mt2
        mt2 = temp
    if (bt1 < mt1 and mt1 < bt2 and bt1 < mt2 and mt2 < bt2) or (mt1 < bt1 and bt1 < mt2 and mt1 < bt2 and bt2 < mt2):
        print('Nothing')
    elif (mt1 - bt2 > 1) or (bt1 - mt2 > 1):
        print('Line')
    elif (bt2 == mt1) or (mt2 == bt1):
        print('Point')
    count = count - 1
    if count == 0:
        break
    
