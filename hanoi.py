#!/usr/bin/python

import random

q1 = sorted([i for i in range(10)])
q2 = []
q3 = []

sticks = [0, q1, q2, q3]


[print(q) for q in q1]

def towerofhanoi(x, pad):
    if len(x) < 3:
        if pad:
            print('move ', x[0], ' to q2')
            print('move ', x[1], ' to q3')
            print('move ', x[0], ' to q3')
        else:
            print('move ', x[0], ' to q1')
            print('move ', x[1], ' to q2')
            print('move ', x[0], ' to q2')
        return
    towerofhanoi(x[:-1], 0)
    if pad:
        print('move ', x[-1], ' to q3')
    else:
        print('move ', x[-1], ' to q2')
    towerofhanoi(x[:-1], 1)

    
                 
towerofhanoi(q1, 1)
