#include "file_cache.h"
#include<semaphore.h>
#include<atomic>
#include<unordered_map>
#include<iostream>
#include<cstdlib>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h> /* mmap() is defined in this header */
#include <fcntl.h>
#include <unistd.h>
#include <mutex>

using namespace std;

// File cache needs a mapping from file name to a Filecache descriptor.
// This implementation uses a hash table with granular range locks; c++
// stl hash map cannot be used directly to achieve granular range locking,
// however if we use a two level hash table then range locking can be implemented
// in the application. We use hash tables in preference to rb tree
// because it has better cache performance due to tighter layout and better
// lookup cost. Hash table has NAME_CACHE_NUM_RANGES maps. 

// round off max_cache_entries to nearest power of two
// TODO: macro-fy?
int ROUND_(int max_cache_entries) {
  int t;
  while(!(max_cache_entries &= (max_cache_entries - 1))) {
    t = max_cache_entries;
  }
  return t << 1;
}

// one range is protected by one lock
// 128 ranges in all
#define NAME_CACHE_NUM_RANGES (1<<7)

enum file_flags {
  NONE  =  0x0,
  RO    =  0x1, // read only
  DIRTY =  0x2,
  VALID =  0x4
};

#define FILE_SIZE (10*(1<<10))

// file descriptor entry
class file_desc {
  std::atomic_int pin_count;
  std::atomic<file_flags> flags;

public:
  char *file_data;

  file_desc(file_flags flag) {
    pin_count.store(1);
    flags.store(flag);
  }

  file_flags setFlagConditional(file_flags flag) {
    file_flags expected = VALID;
    flags.compare_exchange_strong(expected, flag);
    return expected;
  }

  file_flags getFlag() {
    flags.load();
  }

  void rePin() {
    pin_count++; // is atomic by definition
  }

  void unPin() {
    pin_count--; // is atomic by definition
  }

  int getPins() {
    return pin_count.load();
  }

  int MakeValid(const char *file_name) {
    int fdin;
    char *src, *dst;
    struct stat statbuf;

    if ((fdin = open (file_name, O_RDWR)) < 0) {
      if ((fdin = open (file_name, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU)) < 0)
	    goto out;
	else
	  {
	    // Zero fill is ensured automatically by mmap
	    /* go to the location corresponding to the last byte */
	    if (lseek (fdin, FILE_SIZE - 1, SEEK_SET) == -1)
	      goto out;
	    /* write a dummy byte at the last location */
	    if (write (fdin, "", 1) != 1)
	      goto out;
	  }
    }
    if ((file_data = (char *) mmap (0, FILE_SIZE, PROT_READ | PROT_WRITE,
		     MAP_SHARED, fdin, 0)) == (caddr_t) -1)
      goto out;
    flags.store(VALID);
    return 0;
  out:
    close(fdin);
  out1:
    return -1;
  }
  int destroyMap() {
    if (flags.load() == DIRTY)
      msync(file_data, FILE_SIZE, MS_SYNC);
    munmap(file_data, FILE_SIZE);
      
    file_data = NULL;
    flags.store(NONE);
  }
};

// XOR all bits in file name and use last
// 7 bits
int first_level_hash(const char* file_name)
{
  int xor_sum = *(file_name++);
  while(*file_name) {
    xor_sum = xor_sum ^ *(file_name++);
  } 
  return xor_sum % NAME_CACHE_NUM_RANGES;
}

class MyFileCache: public FileCache {
  // unique_pins is the number of unique files in the cache.
  // this is maintained as a semaphore which gives atomic update
  // and synchronisation automatically.
  sem_t unique_pins;
  
  // name_cache is a mapping from a file name to a file descriptor
  std::unordered_map<std::string, class file_desc *> name_cache[NAME_CACHE_NUM_RANGES];
  mutex range_lk[NAME_CACHE_NUM_RANGES];
public:
  MyFileCache(int max_cache_entries):FileCache(max_cache_entries)
    // max_cache_entries_(max_cache_entries)
  {
    // TODO: Assert program's rlimits to see 
    // if max_cache_entries * 10 KB is less than 
    // heap section's hard limits

    sem_init(&unique_pins, 0, max_cache_entries);
  
    // name_cache size is twice the nearest power of two
    // larger than max_cache_entries
    int name_cache_size = ROUND_(max_cache_entries)*2;
    
    // initialise name_cache 
    int i;
    int range_size = name_cache_size/NAME_CACHE_NUM_RANGES;
    for (i=0; i < NAME_CACHE_NUM_RANGES; i++) 
      name_cache[i].rehash(range_size);
  }
  ~MyFileCache() {
    sem_destroy(&unique_pins);
    int i;
    for (i=0; i < NAME_CACHE_NUM_RANGES; i++) {
      name_cache[i].~unordered_map();
      range_lk[i].~mutex();
    }
  }
  void range_lock(int i) {
    range_lk[i].lock();
  }
  void range_unlock(int i) {
    range_lk[i].unlock();
  }
  void PinFiles(const std::vector<std::string>& file_vec) {
    // iterate over file_vec
    int i;
    int first_level_table;
    for (i=0; i<file_vec.size(); i++) {
      // lookup file in name_cache 
      first_level_table = first_level_hash(file_vec[i].c_str());

      range_lock(first_level_table);
      auto got
	= name_cache[first_level_table].find(file_vec[i]);
      range_unlock(first_level_table);
      
      //not found
      if (got == name_cache[first_level_table].end()) {
	file_desc* f = new file_desc(NONE);
	sem_wait(&unique_pins);
	
	range_lock(first_level_table);
	auto ret = 
	  name_cache[first_level_table].emplace(file_vec[i], f);
	if (!ret.second) // we got raced
	  {
	    ret.first->second->rePin();
	    f->~file_desc();
	  }
	else {	
	  f->MakeValid(file_vec[i].c_str());
	}
	range_unlock(first_level_table);
      }
      else {
	// repin
	got->second->rePin();
      }
    }
  }
  void UnpinFiles(const std::vector<std::string>& file_vec) {
       int i;
    int first_level_table;
    for (i=0; i<file_vec.size(); i++) {
      first_level_table = first_level_hash(file_vec[i].c_str());
      range_lock(first_level_table);
      std::unordered_map<std::string, file_desc *>::const_iterator got
	= name_cache[first_level_table].find(file_vec[i]);
      range_unlock(first_level_table);

      //not found
      if (got == name_cache[first_level_table].end()) {
	// Undefined behavior; do nothing for now
	;
      }
      else {
	// unpin
	got->second->unPin();
	int num = got->second->getPins();
	// Assert num > 0 
	if (num==0) {
	  sem_post(&unique_pins);
	  range_lock(first_level_table);
	  name_cache[first_level_table].erase(got->first);
	  range_unlock(first_level_table);
	}
      }
    }
  }

  char *MutableFileData(const std::string& file_name) {
    int  first_level_table;
    first_level_table = first_level_hash(file_name.c_str());
    range_lock(first_level_table);
    std::unordered_map<std::string, file_desc *>::const_iterator got
      = name_cache[first_level_table].find(file_name);
    range_unlock(first_level_table);
    if (got == name_cache[first_level_table].end()) {
      // Undefined behavior; do nothing for now
      ;
    }
    else {
      file_flags ret = got->second->setFlagConditional(DIRTY);
      if (got->second->getFlag() == DIRTY) 
	return got->second->file_data;
      else 
	return NULL;
    }
  }

  const char *FileData(const std::string& file_name) {
    int  first_level_table;
    first_level_table = first_level_hash(file_name.c_str());

    range_lock(first_level_table);
    std::unordered_map<std::string, file_desc *>::const_iterator got
      = name_cache[first_level_table].find(file_name.c_str());
    range_unlock(first_level_table);

    if (got == name_cache[first_level_table].end()) {
      // Undefined behavior; do nothing for now
      ;
    } else {
      // only set flag RO if not dirty
      file_flags ret = got->second->setFlagConditional(RO);
      if (got->second->getFlag() == RO) 
	return got->second->file_data;
      else 
	return NULL;
    }
  }
};





 

