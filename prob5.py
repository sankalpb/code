import sys

L = 0
N = 0

for line in sys.stdin:
    if L == 0:
        L = int(line)
        continue
    if N == 0:
        N = int(line)
        continue
    else:
        W, H = list(map(int, line.split()))
        if W < L or H < L:
            print('UPLOAD ANOTHER')
        else:
            if W == H:
                print('ACCEPTED')
            else: 
                print('CROP IT')
        N = N - 1
        if N == 0:
            break
