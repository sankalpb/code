# Definition for a binary tree node. - https://leetcode.com/problems/recover-binary-search-tree/
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    mismatch1 = None
    mismatch2 = None
    
    def check_node(self, node, predecessor):
        if (node.val < predecessor.val):
            if not self.mismatch1:
                self.mismatch1 = predecessor
                self.mismatch2 = node
            else:
                self.mismatch2 = predecessor
            
    def in_order(self, node, predecessor):
        if predecessor:
            self.check_node(node, predecessor)
        if node.left:
            predecessor = self.in_order(node.left, predecessor)
            self.check_node(node, predecessor)
        else:
            return node
        if node.right:
            predecessor = node
            self.in_order(node.right, predecessor)
            return None
            
    def recoverTree(self, root: TreeNode) -> None:
        """
        Do not return anything, modify root in-place instead.
        """
        self.in_order(root, None)
        temp = self.mismatch1.val
        self.mismatch1.val = self.mismatch2.val
        self.mismatch2.val = temp
        
        
        
        
