#!/usr/bin/python
import sys

n = int(sys.stdin.readline())
superiors = list(map(int, sys.stdin.readline().split()))
node = [None] * (n + 1)
for i, superior in enumerate(superiors):
    if node[i+1] is None:
        node[i+1] = [[]]
    if node[superior] is None:
        node[superior] = [[]]
    if superior == 0:
        root = i + 1
    else:
        node[superior][0].append(i + 1)

#([print('blah', n,i) for n,i in enumerate(node)])

(NONE_FOUND, FIRST_FOUND, SECOND_FOUND, BOTH_FOUND) = (0, 1, 2, -1)

def common_ancestor(n, i, j):
    #print('Now at node ', n, 'for', i, 'and', j)
    state = 0
    if (n == i):
        if common_ancestor(n, sys.maxsize, j) == SECOND_FOUND:
            print('here1' ,i)
            return BOTH_FOUND
        else:
            return FIRST_FOUND
    elif (n == j):
        if common_ancestor(n, i, sys.maxsize) == FIRST_FOUND:
            print('here2', j)
            return BOTH_FOUND
        else:
            return SECOND_FOUND
    else:
        children = node[n][0]
        #print(children)
        for child in children:
            ret = common_ancestor(child, i, j)
            #print(child, i,j, ' returned ', ret)
            if ret == BOTH_FOUND:
                return ret
            elif ret:
                state = state | 1 << ret
        #print('state for ', n, i, j, '=', state)
        if state == (1 << FIRST_FOUND | 1 << SECOND_FOUND):
            print('here3', n)
            return BOTH_FOUND
        else:
            if (state == 1 << FIRST_FOUND):
                return FIRST_FOUND
            elif (state == 1 << SECOND_FOUND):
                return SECOND_FOUND
            else:
                return NONE_FOUND

def predecessor(i):
    pass

def successor(i):
    pass

common_ancestor(12, 23, 17)

