#https://www.hackerearth.com/problem/algorithm/nice-arches-1/

import sys

N = int(sys.stdin.readline())
count = 0
for i in range(N):
    word = sys.stdin.readline().strip()
    if len(word) % 2 != 0:
        continue
    list2 = []
    for letter in word:
        if list2 and list2[-1] == letter:
            list2.pop()
        else:
            list2.append(letter)
    if not list2:
        count += 1
        
print(count)
