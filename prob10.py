import sys

for line in sys.stdin:
    if len(line) > 1:
        (n, k) = map(int, line.split())
        s = [0]*(n+1)
        s[k] = 1 # T(k, k) = 1
        for i in range(k + 1, n + 1):
            s[i] = 2*s[i-1] + pow(2, (i - k - 1)) - s[i-1-k]
        a = s[n]    
        b = pow(2, n)
        while (a % 2 == 0):
            a = a/2
            b = b/2
        print "%d%c%d" % (a, '/', b)
    
