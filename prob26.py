#https://www.hackerearth.com/problem/algorithm/promotion-1/
import sys

n, m = [int(x) for x in sys.stdin.readline().split()]
weights = [int(x) for x in sys.stdin.readline().split()]
trucks = [int(x) for x in sys.stdin.readline().split()]
weights.sort(reverse=True)
trucks.sort(reverse=True)

w = 0
t = 0
count = 1
while (weights):
    #print(w, len(weights))
    if t == len(trucks) or w == len(weights):
        t = 0
        count += 1
        w = 0
        continue
    #print(w, t, count)
    if (weights[w] <= trucks[t]):
        #w += 1
        weights.pop(w)
        t += 1
    else:
        start = w
        end = len(weights) - 1
        while(1):
            if end - start < 2:
                if weights[end] <= trucks[t] and weights[start] >= trucks[t]:
                    res = end
                else:
                    res = -1
                break
            mid = int((start + end)/2)
            #print(start, end, mid, weights[mid], weights[mid + 1], trucks[t])
            if weights[mid] >= trucks[t] and weights[mid + 1] <= trucks[t]:	
                res = mid
                break
            elif weights[mid] < trucks[t] and weights[mid + 1] < trucks[t]:
                end = mid 
            elif weights[mid] > trucks[t] and weights[mid + 1] > trucks[t]:
                start = mid + 1
        if res == -1:
            t = 0
            count += 1
            w = 0
        else:
            #print('hived', w + 1, res + 1)
            #print('weights of ', res + 1, weights[res + 1], trucks[t])
            w = res + 1
        

print((count - 1)*2 + 1)
