import random
import time
from toposort import toposort
import networkx as nx

horses = [(i+1, random.random(), 0) for i in range(25)]
graph = {horse[0]: set() for horse in horses}

def race(h1, h2, h3, h4, h5):
    res = sorted([h1, h2, h3, h4, h5], key = lambda x: x[1])
    graph[res[1][0]].add(res[0][0])
    graph[res[2][0]].add(res[1][0])
    graph[res[3][0]].add(res[2][0])
    graph[res[4][0]].add(res[3][0])

def main():
    random.seed(time.time())

    adj_list = []
    winners = []

    #first 5 races
    for i in range(5):
        new_edges = race(horses[5*i], horses[5*i+1], horses[5*i+2], horses[5*i+3], horses[5*i+4])

    print(list(toposort(graph)))
    
    
    winners = list(toposort(graph))[-1]

    #race #6: winner of winners
    race(*tuple([horses[i-1] for i in winners]))



    print(list(toposort(graph)))
    [print(g.items()) for g in graph]
    

    exit(0)



if __name__ == "__main__":
    main()
