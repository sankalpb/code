#!/usr/bin/python2

def fibonacci(n):
    f1 = 1
    f2 = 2
    i = 4
    while i < n:
        i += 1
        temp = f2 % 1000000007
        f2 = (f1 + f2)  % 1000000007
        f1 = temp % 1000000007
    return f2


print fibonacci(1000000008)
        
        
