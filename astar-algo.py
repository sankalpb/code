import sys
import time

#@profile
def compute():
 a_result = [[0 for i in range(26)] for j in range(26)]
 tmp = [[0 for i in range(26)] for j in range(26)]

 index_computed = 0

 a_index = [[[0 for i in range(26)] for j in range(26)] for i in range(24)]
 for i in range(26):
     line = sys.stdin.readline().split()
     for j in range(26):
         a_index[0][i][j] = int(line[j])


 count = int(sys.stdin.readline())

 for i in range(count):
     C,L = sys.stdin.readline().split()
     L = int(L) - 1
     C = ord(C) - ord('a')

     # a_result = identity matrix
     for i in range(26):
         for j in range(26):
             a_result[i][j] = (i == j) and 1 or 0


     index = 0
     while(L):
         if not index_computed & (1 << index):
             if index == 0:
                 pass
             else:
                 # multiply a_index and a_index
                 for a in range(26):
                     for b in range(26):
                         s = 0
                         for c in range(26):
                             s = (s + a_index[index-1][a][c]*a_index[index-1][c][b]) % 1000000007
                         a_index[index][a][b] = s

             index_computed |= 1 << index
         if ((1 << index) & L):

             #multiply a_result and a_index
             for a in range(26):
                 for b in range(26):
                     s = 0
                     for c in range(26): 
                         s = (s + a_result[a][c]*a_index[index][c][b]) % 1000000007
                         #s = sum(a_result[a][c]*a_index[index][c][b] for c in range(26))
                     tmp[a][b] = s
             a_result, tmp = tmp, a_result
             L = L & (L - 1)
         index = index + 1


     #[print(row) for row in a_result]
     s = 0
     for i in range(26):
         s = (s + a_result[i][C]) % 1000000007
     print(s)


if __name__ == "__main__":
    compute()
