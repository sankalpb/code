import sys
t = int(sys.stdin.readline())
for test in range(t):
    n = int(sys.stdin.readline())
    first = tuple(map(int, sys.stdin.readline().split()))
    second = tuple(map(int, sys.stdin.readline().split()))
    if sum(first) < sum(second):
        first, second = second, first
    happiness = 0 - first[1] - second[1]
    for girl in range(2, n):
        score = tuple(map(int, sys.stdin.readline().split()))
        happiness = happiness - score[1]
        if sum(score) > sum(first):
            second = first
            first = score
        elif sum(score) > sum(second):
            second = score
    print happiness + sum(first) + sum(second)
