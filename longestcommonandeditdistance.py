#!/usr/bin/python




def longestcommon(a, b):
    # longestcommon(a, b) = longestcommon(a[:-1], b[:-1]) + 1 if a[-1] = b[-1]
    #                     = 0 if a[-1] != b[-1]
    m = []
    maxlen = 0
    maxstart = 0

    for i in range(len(a)):
        m.append([])
        
    for i, x  in enumerate(a):
        for j, y in enumerate(b):
            if x == y:
                if i == 0 or j == 0:
                    m[i].append(1)
                else:
                    m[i].append(m[i-1][j-1] + 1)
                if m[i][j] > maxlen:
                    maxlen = maxlen + 1
                    maxstart = i - maxlen + 1
            else:
                m[i].append(0)
    return a[maxstart: maxstart + maxlen]

cost = {
    'drop': 1,
    'edit': 1,
    'add': 1,
    }
                
def editdistance(a, b):
    m = []

    for i in range(len(a)):
        m.append([])

    for i, x  in enumerate(a):
        for j, y in enumerate(b):
            if i > 0 and j > 0:
                if a[i] == b[j]:
                    m[i].append(m[i-1][j-1])
                else:    
                    temp = min((m[i][j-1] + cost['add']),
                               (m[i-1][j-1] + cost['edit']),
                               (m[i-1][j] + cost['drop']))
                    m[i].append(temp)
            elif i > 0 and j == 0:
                m[i].append(m[i-1][0] + cost['drop'])
            elif j > 0 and i == 0:
                m[i].append(m[0][j-1] + cost['add'])
            elif i==0 and j==0:
                if a[i] != b[j]:
                    m[i].append(1)
                else:
                    m[i].append(0)
            else:
                print('error')
    [print(row) for row in m]
        


a = 'kitten'
b = 'kitting'

print(longestcommon(a, b))
print(editdistance(a, b))
