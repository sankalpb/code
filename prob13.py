import sys
import math
# from bitarray import bitarray

def next_power_of_two(x):
    return(int(pow(2, int(math.log(x - 1, 2)) + 1)))
    
def prev_power_of_two(x):
    return(int(pow(2, int(math.log(x - 1, 2)))))


class segmenttree:
    def __init__(self, n):
        self.heap = [None] * (2*next_power_of_two(n)-1)
        self.start_count = [None] * (2*next_power_of_two(n)-1)

        self.outdated = [False] * (2*next_power_of_two(n)-1)
        #self.outdated.setall(False)
        self.ln = len(self.heap)
        # print('ln = ', self.ln, ' n = ', n)
        for i in range(int(len(self.heap)/2) + n, len(self.heap)):
            #print('Zeroing ', i)
            self.start_count[i] = self.heap[i] = 0
        for i in range(int(len(self.heap)/2), int(len(self.heap)/2 + n)):
            #print('Waking ', i)
            self.start_count[i] = self.heap[i] = 1
        for i in reversed(range(int(self.ln/2))):
            #print('Counting ', i)
            self.start_count[i] = self.heap[i] = self.heap[2*i + 1] + self.heap[2*i + 2]

    def aligned_list(self, q, q2):
        # create aligned power of two from within i j
        (i, j) = q[0]
        q.remove(q[0])
        diff = j - i + 1
        aligned_diff = int(pow(2, int(math.log(diff, 2))))
        start = int(self.ln/2) + 1
        while (aligned_diff >= 1):
            if (i & (aligned_diff - 1)) != 0 :
                nearest_boundary = (i + aligned_diff - (i & (aligned_diff - 1)))
            else:
                nearest_boundary = i
            #print('test ', i, j, nearest_boundary, nearest_boundary + aligned_diff - 1)
            if (nearest_boundary + aligned_diff - 1 <= j) :
                    ancestor = (start + nearest_boundary - 1) >> int(math.log(aligned_diff, 2))
                    q2.append((ancestor, aligned_diff))
                    # print('switch on ', nearest_boundary, nearest_boundary + aligned_diff - 1)
                    if (i < nearest_boundary):
                        q.append((i, nearest_boundary - 1))
                        #print('append ', (i, nearest_boundary - 1))
                    if nearest_boundary + aligned_diff - 1 < j :
                        q.append((nearest_boundary + aligned_diff, j))
                        #print('append2 ', (nearest_boundary + aligned_diff, j))
                    return
            aligned_diff = aligned_diff >> 1

    def ancestor(self, i):
        if i == 0:
            return 0
        return int((i + 1)/2) - 1

    def child1(self, i):
        if (i + 1) * 2 - 1 < self.ln:
            return (i + 1) * 2
        else:
            return 0
            

    def child2(self, i):
        if (i + 1) * 2 < self.ln:
            return (i + 1) * 2
        else:
            return 0
    


    def is_outdated(self, i):
        return self.outdated[i]
    
    def find_non_outdated_ancestor(self, i):
        min = -1
        while ( i != 0 ):
            if self.is_outdated(i):
                min = i
            i = self.ancestor(i)
        if min == -1: 
            return -1
        else:
            return self.ancestor(min)

    def do_count(self, x):
        a = self.find_non_outdated_ancestor(x)
        if a != -1:
            t = x
            while (t != a):
                if self.heap[a] != 0:
                    self.heap[t] = self.start_count[t]
                else:
                    self.heap[t] = 0
                self.outdated[t] = False
                t = self.ancestor(x)
        return self.heap[x]
            
    def update_count(self, x, count, on):
        prev_count = self.do_count(x)
        if on:
            self.heap[x] = count
            diff = count - prev_count
            i = self.ancestor(x)
            while (1):
                self.heap[i] = self.heap[i] + diff
                # print('Set ', i, self.heap[i])
                if i == 0:
                    break
                i = self.ancestor(i)
        else:
            self.heap[x] = 0
            diff = - prev_count
            i = self.ancestor(x)
            while (1):
                self.heap[i] = self.heap[i] + diff
                # print('Set ', i, self.heap[i])
                if i == 0:
                    break
                i = self.ancestor(i)

        self.outdated[x] = False
        if self.child1(x):
            self.outdated[self.child1(x)] = True
            self.outdated[self.child2(x)] = True
            
    def range_update(self, i, j, on):
        # print("range_update ", i, j, on)
        q = [(i-1, j-1)]
        q2 = []
        while q:
            self.aligned_list(q, q2)
        for (x, count) in q2:
            self.update_count(x, count, on)    

    def range_query(self, i, j):
        q = [(i-1, j-1)]
        q2 = []
        while q:
            self.aligned_list(q, q2)
        s = 0
        for (x, count) in q2:
            s = s + self.do_count(x)
        return s

    def pr(self, j):
        i = j - int(self.ln/2) - 1
        return (self.heap[i], self.start_count[i], self.outdated[i])
        # [print(i, x, self.outdated[i]) for i,x in enumerate(self.heap)]


                

def startn(x):
    return node[x][3]

def endn(x):
    return node[x][4]

def setstartn(x, val):
    node[x][3] = val

def setendn(x, val):
    node[x][4] = val

def count(x):
    return node[x][1]

def wakecount(x):
    return node[x][2]

def setcount(x, val):
    node[x][1] = val
    setwakecount(x, val)

def setwakecount(x, val):
    #print('setw ', x, val)
    node[x][2] = val

def children(x):
    return node[x][0]

def parent(x):
    return superiors[x-1]




def propagatediff(soldier, diff):
    while parent(soldier) != 0:
        #print(parent(soldier), diff)
        setwakecount(parent(soldier), wakecount(parent(soldier)) + diff)
        soldier = parent(soldier)

def actionneeded(l, i, end):
    soldier = l[i][1]
    while parent(soldier) != 0 and i + 1 < end:
        soldier = parent(soldier)
        print(soldier, [x[1] for x in l[i+1: end]])
        if soldier in [x[1] for x in l[i+1: end]]:
            return False
    return True
    
def fixupcount(ord):
    soldier = ord[1]
    diff = 0
    if ord[0] == 1:
        diff = count(soldier) - wakecount(soldier)
        #print(soldier, diff)
        setwakecount(soldier, count(soldier))
    elif ord[0] == 2:
        diff = -wakecount(soldier)
        #print(soldier, diff)
        setwakecount(soldier, 0)
    else:
        print('blah', order[0])
    if diff:
        propagatediff(soldier, diff)    

 

        
def computecount(start):
    path = [start]
    subcount = []
    j = 1
    while True:
        if not path:
            break
        tail = path[-1]
        # print(tail, end=' ')
        if count(tail) == -1:
            setcount(tail, 0)
            setstartn(tail, j + 1)
            j = j + 1
            if children(tail):
                path.extend(children(tail))
            else:
                path.pop()
                subcount.append(1)
                setendn(tail, j - 1)
        else:
            s = 0
            for i in children(tail):
                s = s + subcount.pop() 
            setcount(path.pop(), s)
            subcount.append(s+1)
            setendn(tail, j - 1)


n = int(sys.stdin.readline())
superiors = list(map(int, sys.stdin.readline().split()))
node = [None] * (n + 1)
for i, superior in enumerate(superiors):
    if node[i+1] is None:
            node[i+1] = [[], -1, 1, -1, -1]
    if node[superior] is None:
        node[superior] = [[], -1, 1, -1, -1]
    if superior == 0:
        root = i + 1
    else:
        node[superior][0].append(i + 1)

computecount(root)
# print()
t = segmenttree(n)


q = int(sys.stdin.readline())
orders = []
for i in range(q):
    orders.append(list(map(int, sys.stdin.readline().split())))
# print(orders)

start = 0
end = 0
for order in orders:
    # print(order)
    i = node[order[1]][-2]
    j = node[order[1]][-1]

    if order[0] != 3:
        t.range_update(i, j, -order[0] + 2)
        # print(t.range_query(2, 31))

    else:
        print(t.range_query(i, j))
        #[print(i, n, t.pr(startn(i))) for i, n in enumerate(node)]


    
    
#[print(n) for n in node]

    


    
