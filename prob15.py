#!/usr/bin/python
import sys

line = sys.stdin.readline().split()
(k, l) = (int(line[0]), int(line[1]))
n = int(sys.stdin.readline())
klist = list(map(int, sys.stdin.readline().split()))
table =  [None] * 100000
s = k
q = [(s, 0)]
table[s] = 1
while q:
    head = q.pop(0)
    key = head[0]
    depth = head[1]
    for kel in klist:
        newkey = (kel * key) % 100000
        if newkey == l:
            print(depth + 1)
            exit(0)
        if not table[newkey]:
            table[newkey] = 1
            q.append((newkey, depth + 1))
print(-1)    

