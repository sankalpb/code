#/usr/bin/python

'''
Interface robot
{
CanGo() //checks to see if it can travel in the direction it is facing.
TurnRight() // changes direction
IsAtFinish() //returns true if standing at the end of the maze.
Go() //moves 1 step in the direction it is facing.
}

r - starting point. 
X - blocked area. 
f - finish. 
'''

smatrix = """
X	X	X	X	X	X	X	X
X	X	o	o	o	o	o	X
r	o	o	X	o	X	o	X
X	o	X	X	o	X	o	X
X	o	X	X	o	o	o	X
X	o	X	X	X	o	X	X
X	o	X	X	X	f	X	X
X	X	X	X	X	X	X	X
"""

smatrix_new = smatrix.split('\n')

def edgelist(i, j):
    a = 0
    if i - 1 >= 0:
        if matrix[i-1, j] == 0:
            a |= 1
    if j + 1 < len(matrix[i]):
        if matrix[i, j + 1] == 0:
            a |= 2
    if i + 1 < len(matrix):
        if matrix[i + 1 , j + 1] == 0:
            a |= 4
    if j - 1 >= 0:
        if matrix[i, j - 1] == 0:
            a |= 8
    return (a & 1, a & 2, a & 4, a & 8)
    
                

i = 0
matrix = []
visited = []
parent = []
for line in smatrix_new:
    if not line.split():
        continue
    matrix.append([])
    visited.append([])
    parent.append([])
    j = 0
    for element in line.split():
        if element == 'X':
            matrix[i].append(0)
        else:
            matrix[i].append(1)
            if element == 'r':
                start = (i, j)
            if element == 'f':
                end = (i, j)
                # change this
                # matrix[i][-1] = 0
                
        visited[i].append(0)
        parent[i].append([-1, -1])
        j = j + 1
    i = i + 1


path = []
def bfs(start, end):
    q = [start]
    visited[start[0]][start[1]] = 1
    while q:
        n = q.pop(0)
        (i, j) = n
        if n == end:
            print('end found', i, j)
            print(parent)
            par = (0, 0)
            while(par != start):
                par = parent[i][j]
                print(par)
                (i, j) = par
            break
        if i - 1 >= 0:
            if matrix[i-1][ j] != 0 and visited[i-1][ j] == 0:
                q.append((i-1, j))
                visited[i-1][j] = 1
                parent[i-1][j] = n
        if j + 1 < len(matrix[i]):
            if matrix[i][ j + 1] != 0 and visited[i][ j + 1] == 0:
                q.append((i, j+1))
                visited[i][ j + 1] = 1
                parent[i][ j + 1] = n
        if i + 1 < len(matrix):
            if matrix[i + 1][ j] != 0 and visited[i + 1 ][ j] == 0: 
                q.append((i + 1, j))
                visited[i + 1 ][ j] = 1
                parent[i + 1 ][ j] = n
        if j - 1 >= 0:
            if matrix[i][ j - 1] != 0 and visited[i][ j - 1] == 0:
                q.append((i, j - 1))
                visited[i][ j - 1] = 1
                parent[i][ j - 1] = n
        visited[n[0]][n[1]] = 2
        

        


    
#print(matrix)
#print(visited)
#print(parent)
bfs(start, end)
