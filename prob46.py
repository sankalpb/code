class Solution:
    dest = list(range(10))
    dest[0] = (4,6)
    dest[1] = (6,8)
    dest[2] = (7,9)
    dest[3] = (8,4)
    dest[4] = (3,9,0)
    dest[5] = ()
    dest[6] = (7,1,0)
    dest[7] = (2,6)
    dest[8] = (3,1)
    dest[9] = (4,2)
    
    
    
    def knightDialer(self, N: 'int') -> 'int':
        answers = [[] for i in range(10)]
        for i in range(10):
            answers[i] = list(range(N))    
            answers[i] = [1]
            answers[i].extend([0 for i in range(N-1)])
            
        for i in range(1, N):
            for j in range(0, 10):
                for d in self.dest[j]:
                    answers[j][i] = (answers[j][i] + answers[d][i-1]) % (pow(10, 9) + 7)
        sum = 0            
        for i in range(10):
            sum = (sum + answers[i][N-1]) % (pow(10, 9) + 7)
        return sum
                    
solution = Solution()
print(solution.knightDialer(2))
