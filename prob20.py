## https://www.hackerearth.com/problem/algorithm/shil-and-palindrome-research/
import sys

#@profile
def is_palindrome(cmap):
    odd  = [x for x in cmap if x & 1 == 1]
    count = sum(cmap)

    if count % 2 == 0 and len(odd) != 0:
        return False
    if count % 2 != 0 and len(odd) != 1:
        return False
    return True


#@profile     
def level1_to_cmap(s, index, cmap):
        if index * 2 < len(s):
            cmap[s[index * 2] - ord('a')] += 1
        if index * 2 + 1 < len(s):
            cmap[s[index * 2 + 1] - ord('a')] += 1

#@profile     
def leveln_to_cmap(s, index, cmap, cmap_left, cmap_right):
        for i in range(26):
            cmap[i] = cmap_left[i] + cmap_right[i]

#@profile       
def seg_tree(s, root, dep):
    level = 0
    while(dep):
        dep -= 1
        for i in range(pow(2, dep)):
            cmap = [0 for j in range(26)]
            if level == 0:
                level1_to_cmap(s, i, cmap)
            else:
                leveln_to_cmap(s, i, cmap, root[dep + 1][2 * i], root[dep + 1][2 * i + 1])
            root[dep].append(cmap)
        level = 1
        # print("added", len(root[dep]), "nodes to level", dep)

#@profile        
def update_tree(root, index, l1, l2, dep):
    while (dep):
        dep -= 1
        index = int(index / 2)
        root[dep][index][l1] -= 1
        root[dep][index][l2] += 1
    # print(root[0][0])

# indexes should be zero based        
#@profile
def accumulate_cmaps(s, root, index1, index2, dep):
    state = 0
    cmap = [0 for i in range(26)]
    cmap[s[index1] - ord('a')] += 1
    cmap[s[index2] - ord('a')] += 1
    level = 0
    while(dep):
        dep -= 1
        level += 1
        if state == 0:
            if index1 & (1 << dep) == index2 & (1 << dep):
                continue
            else:
                state = 1
                continue
        if state == 1:
            if index2 & (1 << dep):
                #accumulate cmaps dep 1 << dep - 1
                # print("accumulating level", level, "index", (index2 >> dep) - 1)
                if dep == 0:
                    cmap[s[index2 - 1] - ord('a')] += 1
                else:
                    lindex2 = (index2 >> dep) - 1
                    for i in range(26):
                        cmap[i] += root[level][lindex2][i]
            if not index1 & (1 << dep):
                #accumulate_cmaps dep 1 << dep
                # print("accumulating level", level, "index", (index1 >> dep) + 1)
                if dep == 0:
                    cmap[s[index1 + 1] - ord('a')] += 1
                else:
                    lindex1 = (index1 >> dep) + 1
                    for i in range(26):
                        cmap[i] += root[level][lindex1][i]
    return cmap

#@profile
def compute():
    N, Q = [int(x) for x in sys.stdin.readline().split()]
    S = bytearray(sys.stdin.readline().strip(), "ascii")
    root = []
    l = N - 1
    dep = 0
    while (l):
        dep += 1
        l = l >> 1
    root = [[] for i in range(dep)]
    seg_tree(S, root, dep)

    for i in range(Q):
        command = sys.stdin.readline().split()
        # print(command)
        if command[0] == "1":
            index = int(command[1]) - 1
            l2 = ord(command[2]) - ord('a')
            l1 = S[index] - ord('a')
            S[index] = ord(command[2][0])
            update_tree(root, index, l1, l2, dep)
        if command[0] == "2":
            L = int(command[1]) - 1 
            R = int(command[2]) - 1
            cmap = accumulate_cmaps(S, root, L, R, dep)
            # print(cmap)
            if is_palindrome(cmap):
                print('yes')
            else:
                print('no')

if __name__ == "__main__":
    compute()
    
