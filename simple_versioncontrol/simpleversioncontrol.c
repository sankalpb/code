/* simple version control: Supports versioning of files
   where only these operations are permitted between
   subsequent commits:
   1. Appending a new line at the end of the file.
   2. Deleting any existing line.
   Metafiles:
   .version: current version of file. Text file.
   .journal: maintain diff of two version as a record of
             16 bytes. This is a text file of the format 
	     d <line no.> 
	     a <num_chars> <line to be appended>
   .chkpoint_<n>: keep a checkpoint which is a snapshot of file
                  at every 20th commit so that versions can be
		  generated deterministically with atmost 20 
		  operations.
   All metafiles are ASCII text and human readable.
*/



#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

#define CHECKPOINT_INTERVAL 20
enum {
	SUCCESS,
	ENOEXIST,
	EINVALID,
	ECREATE
} errors;

char error_string[4][40] = {
	"",
	"Metafile not found",
	"Invalid operation requested",
	"Cannot create file in directory"
};

/* keep an incore file, 500 bytes should be enough at all times 
   given 20 lines of 10 chars each */
int filesize = 500;
char incore_file[500];

char padded_record[16];

void usage(void) {
	printf("Usage:\n");
	printf("svc <filename> \n");
	printf("svc version\n");
	exit (8);
}

/* return a diff record between fh and incore_file */
char *find_diff(FILE* fh)
{
	char c;
	int n = 0; 
	int i = 0;
	memset(padded_record, ' ', 15);
	padded_record[15] = '\n';
	while((i < filesize) && (c = fgetc(fh)) != EOF) {
		if (c == '\n')
			n++;

		if (c != incore_file[i++]) 
			break;
	}
	if ((i == filesize) && (c != EOF)) {
		int num;
		fscanf(fh, "%[^\n]", &padded_record[5]);
		num = strlen(&padded_record[5]);
		padded_record[num + 5] = ' ';
		/* truncate line at 10th char */
		sprintf(padded_record, "a %d ", num % 10);
		padded_record[4] = ' ';
		return padded_record;
	}
	if ((i < filesize) && (c != EOF)) {
		sprintf(padded_record, "d %d ", n + 1);
		padded_record[4] = ' ';
		return padded_record;
	}
	return NULL;
}

int apply_diff(char* diff_record) {
	int n; 
	int num;
	switch (diff_record[0])	{
		case 'd' :
			n = atoi(&diff_record[2]) - 1;
			int marker1 = 0, marker2 = 0, i = 0;
			while(n != -1) {
				if (incore_file[i++] == '\n') {
					n--;
					if (n == 0) marker1 = i;
					if (n == -1) marker2 = i;
				}
			}
			while(marker2 < filesize)
				incore_file[marker1++] = incore_file[marker2++];
			filesize -= (marker2 - marker1);
			break;
		case 'a':
			sscanf(padded_record, "a %d ", &num);
			strncpy(&incore_file[filesize], &diff_record[5], num);
			filesize += num;
			incore_file[filesize++] = '\n';
			break;
		default:
			return -EINVALID;
		}
}
/* generate version of file given by version in the char buffer
   incore_file */
int generate_version(int version) {
	char temp_buf[256];
	int generated_file;
	FILE* version_fh;
	int cur_version = -1;
	
	bzero(incore_file, 500);
	filesize = 0;
	
	if (version == -1)
		return 0;

	version_fh = fopen(".version", "r");
	if ((version_fh) == NULL) {
		return -ENOEXIST;
	}
	else {
		if (fscanf(version_fh, "%d", &cur_version));
		if (cur_version < version) 
			return -EINVALID;
	}

	/* read nearest checkpointed file and apply diff 
	   one by one */
	int nearest_cp_version = version - version % CHECKPOINT_INTERVAL;
	sprintf(temp_buf, ".chkpnt_%d", nearest_cp_version);
	FILE *nearest_cp_fh;
	nearest_cp_fh = fopen(temp_buf, "r");
	if (!nearest_cp_fh) { 
		return -ENOEXIST;
		/* Good to have: go back to a still previous 
		 checkpointed file and apply journal log */
	}
	filesize = fread(incore_file, 1, 500, nearest_cp_fh);
	filesize--;
	fclose(nearest_cp_fh); 

	/* open journal file and skip logs for until nearest
	   checkpointed file */
	FILE *journal_fh = fopen(".journal", "r");
	if (journal_fh == NULL)
		return -ENOEXIST;
	fseek(journal_fh, 0, (nearest_cp_version - 1) * 16);
	while(nearest_cp_version != version)
		{
			if (fscanf(journal_fh, "%16c" ,padded_record) != EOF)
				apply_diff(padded_record);
			nearest_cp_version++;
		}
	fclose(journal_fh);
}

int commit(FILE* file) {
	int chkpnt_file;
	char temp_buf[256];
	int version = -1;
	/* By implication of the svc <version> syntax only
	   one version controlled file can exist in a workspace
	   and hence only one version file */
	FILE *version_cur = fopen(".version", "r");
	if (version_cur != NULL) {
		fscanf(version_cur, "%d", &version);
		fclose(version_cur);
	}

	/* find difference between current version and file */
	generate_version(version);
	char* record = find_diff(file);
	
	/* append record  to journal */
	if (record != NULL) {
		FILE *version_new = fopen(".version_new", "w");
		if (version_new == NULL)
			return -ECREATE;
		fprintf(version_new, "%d\n", version + 1);
		unlink(".version");
		fclose(version_new);
		rename(".version_new", ".version");
		

		FILE* journal_fh = fopen(".journal", "a+");
		if (journal_fh == NULL)
			return -ECREATE;
		if (version >= 0) {
			fprintf(journal_fh, "%.16s", record);
		}
		fclose(journal_fh);

		/* checkpoint file for every 20th commit */
		if (((version + 1) % CHECKPOINT_INTERVAL) == 0) {
			int size;
			fseek(file, 0, SEEK_SET);
			sprintf(temp_buf, ".chkpnt_%d", version + 1);
			FILE* dest = fopen(temp_buf, "w+");

			while (size = fread(temp_buf, 1, 256, file)) {
				fwrite(temp_buf, 1, size, dest);
			}

			fclose(dest);
		}
	}
}

int main(int argc, char *argv[]) {

	int ret = 0;
	if (argc > 1) {
		if ((argv[1][0] >= '0') && (argv[1][0] <= '9'))	{
			int version, i;
			version = atoi(argv[1]);
			ret = generate_version(version); 
			for (i=0; i<filesize; i++)
				putchar(incore_file[i]);
		}
		else {
			FILE* file;
			file = fopen(argv[1], "r");
			if (file == NULL)
				printf("File %s does not exist in current workspace\n", argv[1]);
			else
				ret = commit(file);
			fclose(file);

		}
		if (!ret)
			printf("%s\n", error_string[ret]);


	}
	else
		usage();

	return (0);
}
