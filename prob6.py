import sys

def checksoln(C):
    for i, h in enumerate(reversed(heights)):
        if i == 0:
            prev = h + C
            continue
        if prev > h:
            prev = min(prev - 1, h + C)
            continue
        else:
            if (h - C) > (prev - 1):
                return False
            else:
                prev = prev - 1
        if prev < 1: 
            return False
        #print(prev, end='')
    return True
        


#(INCREASING, DECREASING) = (1, 2)
count = int(sys.stdin.readline())
while count > 0:
    cnt = int(sys.stdin.readline())
    line = sys.stdin.readline()
    heights = list(map(int, line.split()))
    prev = 0
    a = 0
    b = (max(heights) - min(heights))/2 + cnt/2 + 1
    if cnt == 1:
        count -= 1
        print(0)
        continue
    while 1:
        mid = int((a + b + 1)/2)
        if a == mid or b == mid:
            break
        #print(a, b, mid, checksoln(mid), heights)
        if checksoln(mid):
            b = mid
        else:
            a = mid
    print(mid)

    count -= 1
