import sys

file = open('input')
for line in file:
    denominator = 2*int(line)
    numerator = 1 
    print('0.', end='')
    rem = -1
    while(rem != 0):
        numerator = numerator*10
        while numerator < denominator:
            numerator = numerator*10
            print('0', end='')
        else: 
            div = numerator / denominator
            rem = numerator % denominator
            print(int(div), end='')
            numerator=rem
    print()
