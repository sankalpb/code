# https://projecteuler.net/problem=67
import sys

triangle = []
level = 0
for line in sys.stdin:
    l = [int(x) for x in line.split()]
    triangle.append(l)

levels = len(triangle)
maxtriangle = [[0 for _ in range(i + 1)] for i in range(levels)]

left = right = 0
for level in range(levels - 1, -1, -1):
    print(level)
    for index in range(level + 1):
        if level != levels - 1:
            left = maxtriangle[level + 1][index]
            right = maxtriangle[level + 1][index + 1]
        maxtriangle[level][index] = triangle[level][index] + max(left, right)

print(maxtriangle[0][0])
