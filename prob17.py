#!/usr/bin/python2

import sys

t = int(sys.stdin.readline())
for i in range(t):
    l = int(sys.stdin.readline())
    endpoints = [l]
    uppers = [int(ln) for ln in sys.stdin.readline().split()]
    lowers = [int(ln) for ln in sys.stdin.readline().split()]
    endpoints.extend([ln + c + 1 for c, ln in enumerate(uppers)])
    endpoints.extend([ln + c + 1 for c, ln in enumerate(lowers)])
    print max(endpoints)
