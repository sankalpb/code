#include <time.h>
#include <stdlib.h>
#include <stdio.h>


typedef struct list {
	int data;
	struct list *next;
} list;


void printlist(list* l) {
	while(l) {
		printf("%d ", (l->data));
		l = l->next;
	}
	printf("\n");
		
}

list* enqueue(list *tail, list *item) {
	tail->next = item;
	return tail->next;
}

list *dequeue(list *head, list *item) {
	item->data = head->data;
	return head->next;
}

void reverselist(list **head, list **tail) {
	list *one = NULL, *two = NULL, *three = NULL;
	one = *head;
	if (!one)
		return;
	if (one->next)
		two = one->next;
	else
		return;
	while(1) {
		if (!two)
			break;
		three = two->next;
		two->next = one;
		one = two;
		two = three;
	}
	*tail = *head;
	*head = one;
	(*tail)->next = NULL;
}

int main() {
	srand(time(NULL));
	int i = 0;
	list *head, *tail;
	list items[100];
	for(i = 0; i < 15; i++) {
		list* item = &items[i];
		if (i == 0) {
			head = tail = item;
			continue;
		}
		
		int r = rand();
		item->data = r;
		item->next = NULL;
		tail = enqueue(tail, item);
	}
	printlist(head);
	reverselist(&head, &tail);
	head = tail = NULL;
	reverselist(&head, &tail);
	head = tail = &items[0];
	reverselist(&head, &tail);
	printlist(head);
		

	
}
       
