class Solution:
    letters = dict()
    letters['2'] = ('a', 'b', 'c')
    letters['3'] = ('d', 'e', 'f')    
    letters['4'] = ('g', 'h', 'i')
    letters['5'] = ('j', 'k', 'l')
    letters['6'] = ('m', 'n', 'o')
    letters['7'] = ('p', 'q', 'r', 's')
    letters['8'] = ('t', 'u', 'v')
    letters['9'] = ('w', 'x', 'y', 'z')

    answers = []
        
    def _letterCombinations(self, digits, index, answer):
        if index < 0:
            return self.answers
        digit = digits[index]
        for letter in self.letters[digit]:
            answer[index] = letter
            if index == 0:
                self.answers.append("".join(answer))
            else:    
                self._letterCombinations(digits, index - 1, answer)
        return self.answers
        
    def letterCombinations(self, digits: 'str') -> 'List[str]':
        self.answers = []
        n = len(digits)
        answer = list(range(n))
        return self._letterCombinations(digits, n-1, answer)


solution = Solution()
print(solution.letterCombinations(''))
