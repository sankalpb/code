#https://www.hackerearth.com/problem/algorithm/a-to-b-1/
import sys
import math

out = open('output283')

n = int(sys.stdin.readline())
k = int(math.log(n, 2)) + 1

level = [ [] for _ in range(k + 1)]

level[k].extend([int(x) for x in sys.stdin.readline().split()])
level[k].extend([1 for _ in range((1 << k) - n)])
q = int(sys.stdin.readline())
prime = (1000000000 + 7)

p = 1
zcount = 0
for i in range(k - 1, 0, -1):
    ln = 1 << (i)
    level[i] = [0 for _ in range(ln)]
    for j in range(ln):
        #print('(n, k)', n, k, '(i, j)', i, j)
        level[i][j] = (level[i+1][2*j] * level[i + 1][2*j +1]) % prime
#[print(level[j]) for j in range(k + 1)]


for _ in range(q):
    line = sys.stdin.readline()
    if line[0] == '0':
        t, i, v = [int(x) for x in line.split()]
        i = i - 1
        if level[k][i] == v:
            continue
        level[k][i] = v
        t = k - 1
        while(t):
            #print(level[t][i >> 1], end='')
            level[t][i >> 1] = (level[t + 1][(i >> 1) << 1] * level[t + 1][((i >> 1) << 1) + 1]) % prime
            #print('-->', level[t][i >> 1])
            i = i >> 1
            t -= 1
    elif line[0] == '1':
        t, i = [int(x) for x in line.split()]
        i = i - 1
        t = k
        p = 1
        while(t):
            if i % 2 == 0:
                p = (p * level[t][i + 1]) % prime
                #print(level[t][i + 1])
            else:
                p = (p * level[t][i - 1]) % prime
                #print(level[t][i + 1])
            i = i >> 1
            t -= 1
        ans = int(out.readline())
        if p != ans:
            [print(level[j]) for j in range(k + 1)]
            error(0)
        print(ans, p)
        
        
