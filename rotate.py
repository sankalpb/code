#!/usr/bin/python


import random

n =  13 # random.randint(1000, 2000)
p =  3  #random.randint(0, int(n/4))


a = [random.randint(0, 90000) for i in range(n)]


def transform(i):
    return (i + p) % n

cyclestart = 0
count = 0
current = 0
cyclecount = 0
print(a)
while True:
    nxt = transform(current)
    print(current, nxt)
    dislodged = a[nxt]
    a[nxt] = dislodged
    if nxt == cyclestart:
        if cyclecount == 0:
            cyclecount = count
        print('new cycle at ', cyclestart)
    else:
        current = nxt
    count += 1
    if count == n:
        break
    


print(n, p)

print(a)
