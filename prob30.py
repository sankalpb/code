# https://www.hackerearth.com/problem/algorithm/oliver-and-the-game-3/
import sys

n = int(sys.stdin.readline())

neighbors = [ [] for _ in range(n)]

for _ in range(n - 1):
    u, v = [int(x) - 1 for x in sys.stdin.readline().split()]
    neighbors[u].append(v)
    neighbors[v].append(u)

vert = [[0, 0, 0] for _ in range(n)] # visited, start, end
q = [0]
i = 0
while (q):
    v = q[-1]
    if vert[v][0] == 1:
        vert[v][2] = i
        q.pop()
    else:
        vert[v][1] = i
        vert[v][0] = 1
        for n in neighbors[v]:
            if vert[n][0] == 0:
                q.append(n)
    i += 1

q = int(sys.stdin.readline())
for _ in range(q):
    d, x, y = [int(x) - 1 for x in sys.stdin.readline().split()]
    _, start1, end1 = vert[x]
    _, start2, end2 = vert[y]
    if d == 0:
        if start2 <= start1 and end2 >= end1:
            print('YES')
            continue
    else:
        if start2 >= start1 and end2 <= end1:
            print('YES')
            continue
    print('NO')

    

        

