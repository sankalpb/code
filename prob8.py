import sys
from math import *
count = int(sys.stdin.readline())

def eratosthenes2(n):
    sum2 = 0
    multiples = set()

    multiples.update(range(4, n+1, 2))
    for i in range(3, n+1, 2):
        if i not in multiples:
            primes.append(i)
            multiples.update(range(i*i, n+1, i))

def calculateCornerPrime(n):
    for stage in range(3, n+1, 2):
        a = [stage*stage - stage + 1, stage*stage - 2*stage + 2, stage*stage - 3*stage +3]
        for i in primes:
            for j, m in enumerate(a):
                if m % i == 0 and m != i:
                    del(a[j])
        corner_primes.append(len(a))

n = []
corner_primes = []
primes = []
while(count):
    count -= 1
    primeEl = []
    n.append(int(sys.stdin.readline()))
m = max(n)
eratosthenes2(m)    
calculateCornerPrime(m)
#print(primes)
#print(corner_primes)
for i, sz in enumerate(n):
    totalDiagEl = 2*sz - 1
    totalPrime = sum(corner_primes[0: int(sz/2)])
    print("%8f" % round(totalPrime*100/totalDiagEl, 6))

    
 
    
