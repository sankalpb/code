# https://www.hackerearth.com/problem/algorithm/starting-game-development/
import sys

n, m, q = [int(x) for x in sys.stdin.readline().split()]
params = [[] for _ in range(n)]
for param in params:
    param.extend([int(x) for x in sys.stdin.readline().split()])
    
def level(grade, x):
    start = 0
    end = len(grade) - 1

    while (1):
        if end - start < 2:
            if grade[end] <= x:
                return end
            if grade[start] <= x:
                return start
            return 0
        
        mid = int((start + end)/2)
        #print(start, mid, end, 'looking for', x, 'in', grade[mid], grade[mid + 1])
        if grade[mid] <= x and grade[mid + 1] > x:
            return mid
        elif grade[mid] <= x and grade[mid + 1] <= x:
            start = mid + 1
        elif grade[mid] > x:
            end = mid


for _ in range(q):
    player = [int(x) for x in sys.stdin.readline().split()]
    print(min([level(params[i], player[i]) for i in range(n)]) + 1)
