import sys

def sqDistance(a, b):
    return (a[1] - b[1])*(a[1] - b[1]) + (a[0] - b[0])*(a[0] - b[0])

count = int(sys.stdin.readline())
while(count):
    count -= 1
    line = sys.stdin.readline().split()
    rcount = int(line[0])
    ucount = int(line[1])
    r = []
    u = []
    for i in range(rcount):
        line = map(int, sys.stdin.readline().split())
        r.append(tuple(line))
    for i in range(ucount):
        line = map(int, sys.stdin.readline().split())
        u.append(tuple(line))
    d = {}
    d1 = {}
    for user in u:
        d[user] = [sqDistance(user, x), i) for x in r]
    for router in r:
        d[router] = [sqDistance(router, x)) for x in r]
    for user in u:
        for router in r:
            cost[user] = d[user]
    print(d)
    print(d1)
