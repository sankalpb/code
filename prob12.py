import sys

cases = int(sys.stdin.readline())
for case in range(cases):
    packets = int(sys.stdin.readline())
    X = []
    for packet in range(packets):
        X.append(int(sys.stdin.readline()))
    X.sort()
    inner = 0
    outer = 1
    count = len(X)
    while outer < len(X):
        if X[inner] < X[outer]:
            inner = inner + 1
            outer = outer + 1
            count = count - 1
        else:
            outer = outer + 1
    print(count)
            
