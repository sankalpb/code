#https://www.hackerearth.com/problem/algorithm/finding-numbers/
import sys
import math

def countpairs(factors, z, m, n, x):
    if len(factors) == 0:
        if (z/x <= n):
            #print(x, z/x)
            return 1
        else:
            return 0
    factor, count = factors.pop()

    num = 0
    for i in range(count + 1):
        if x <= m:
            print('countpairs', factors, z, m, n, x)
            num += countpairs(factors, z, m, n, x)
        else:
            break
        x = x * factor

    factors.append((factor, count))
    return num

n = int(sys.stdin.readline())
for _ in range(n):
    z, m, n = [int(x) for x in sys.stdin.readline().split()]

    res = 0

    lim = min(int(math.sqrt(z)), max(m, n))
    print(lim)
    z1 = z 
    factors = []
    i = 2
    while(i <= lim and z > 1):
        count = 0
        while z % i == 0:
            count += 1
            z = int(z/i)
        if count:
            factors.append((i, count))
        if i == 2:
              i += 1
        else:
              i += 2
    if z > 1:
        factors.append((z, 1))
    print(factors)
    print('countpairs', factors, z1, m, n, 1)
    res += countpairs(factors, z1, m, n, 1)
    print(res)
    
          
