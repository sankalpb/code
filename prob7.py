import sys

(NORMAL, COMMENT1, COMMENT2, PATTERN1, PATTERN2) = range(5)
s = ''
state = NORMAL
for line in sys.stdin:
    for char in line:
        if state == NORMAL:
            if char == '/':
                s += char 
                state = COMMENT1
                continue
            elif char == '-':
                state = PATTERN1
                continue
            else:
                s += char 
                continue
        if state == PATTERN1:
            if char == '>':
                state = NORMAL
                s += '.'
                continue
            elif char == '/':
                s += '-' + char
                state = COMMENT1
                continue
            elif char == '-':
                s += '-'
                continue
            else:
                s += '-' + char
                state = NORMAL
                continue
        if state == COMMENT1:
            if char == '/':
                # comment start - set state to normal and read next line 
                state = COMMENT2
                s += char
                continue
            elif char == '-':
                state = PATTERN1
            else:
                state = NORMAL
                s += char
                continue
        if state == COMMENT2:
            s += char
            continue
    state = NORMAL
print(s)
    


            
            
            
            

