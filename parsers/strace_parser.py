import sys
import re

f = open(sys.argv[1])
fd_list = []
offset = 0
size = 0
sum = 0
for line in f: 
    if "open(" in line and "dbbench" in line:
        fd_list.append(line.split()[-1])
    if "close" in line:
        fd = line.split()[0][6:-1]
        if fd in fd_list:
            fd_list.remove(fd)
    if 'lseek' in line:
        matchObj = re.match( r'lseek\(([\d]*), ([-\d]*), .*\)', line, re.M|re.I)
        if matchObj and matchObj.group(1) in fd_list:
            offset = matchObj.group(2)
    if 'write' in line:
        matchObj = re.match( r'write\(([\d]*)(.*)', line, re.M|re.I)
        if matchObj and matchObj.group(1) in fd_list:
            size = int(matchObj.group(2).split()[-1])
    #print(offset, size)
    sum += size
print('Total number of blocks written = ', sum/512)
