import sys
import matplotlib
import pylab

blah = 0
part = 'denso'
dev = 'mmcblk0'

if part == 'renesas':
    slc_size = 2
    mlc_size = 4
    slc_region = (1946, 2047)
elif part == 'bmw':
    slc_size = 4
    mlc_size = 8
    slc_region = (3914, 4095)
elif part == 'denso':
    slc_size = 4
    mlc_size = 8
    slc_region = (1925, 2047)

def main(arg, req):
    f = open(arg[0])
    num_erase = []
    pblock = []
    i = 0
    for line in f:
        if 'Erase' in line:
            if line.split()[5] is not '0':
                pblock.append(int(line.split()[5]))
                num_erase.append((int(line.split()[7]), i))
                i = i + 1
        if dev in line:
            bwrite = int(line.split()[5])
    erase_list = sorted(zip(pblock, num_erase), key=lambda x: x[0])
    count = sum([x[1][0] for x in erase_list])

    if len(arg) > 1:
        f1 = open(arg[1])
        num_erase1 = []
        pblock1 = []
        i = 0
        for line in f1:
            if 'Erase' in line:
                if line.split()[5] is not '0':
                    pblock1.append(int(line.split()[5]))
                    num_erase1.append((int(line.split()[7]), i))
                    i = i + 1
            if dev in line:
                bwrite1 = int(line.split()[5])
        #[print(item, item[0] != item[2] and '          #' or ' ' ) for item in (zip(pblock, num_erase, pblock1, num_erase1))]
        erase_list1 = sorted(zip(pblock1, num_erase1), key=lambda x: x[0])
        count1 = sum([x[1][0] for x in erase_list1])
        #[print(erase_tup) for erase_tup in zip(erase_list, erase_list1) if erase_tup[0] != erase_tup[1]]
        # print(count1 - count)
        dic = dict(erase_list)
        dic1 = dict(erase_list1)
        diffdic = {}
        region1 = {}
        region2 = {}
        former = []
        latter = []
        difflist = []
        for key in dic.keys():
            if key in dic1:
                diffdic[key] = dic1[key][0] - dic[key][0]
                former.append(dic[key][1])
                latter.append(dic1[key][1])
                difflist.append(dic1[key][0] - dic[key][0])
                if int(key) >= slc_region[0] and int(key) <= slc_region[1]:
                    region1[key] = dic1[key][0] - dic[key][0]
                else:
                    region2[key] = dic1[key][0] - dic[key][0]
        #[print(item) for item in diffdic.items()]
        #[print(item) for item in region1.items()]
        
        print("%10s%10s%10s%13s%12s%12s" % ("total",  "metadata",  "normal", "bwrite(K)", "WAF1", "WAF"))
        print("%10d%10d%10d" % (sum(diffdic.values()), sum(region1.values())*slc_size, sum(region2.values())*mlc_size), end='')
        bw = (bwrite1-bwrite)/2048
        print("%13.2f%10.2f%10.2f" % ((bwrite1-bwrite)/2, (sum(region2.values())*mlc_size)/req, (sum(region2.values())*mlc_size + sum(region1.values())*slc_size)/req))
        #print("WAF_xse = ", (sum(region2.values())*mlc_size + sum(region1.values())*slc_size)/bw)
        #matplotlib.pyplot.ylim(0, 100)
        matplotlib.pyplot.figure(figsize=(18.5,10.5), dpi=100)
        #for item in diffdic.items():
            #print(item)
        matplotlib.pyplot.scatter(former, difflist, marker = '|')
        matplotlib.pyplot.savefig(arg[0] + '.png')
        matplotlib.pyplot.clf()
        matplotlib.pyplot.scatter(latter, difflist, marker = '|')
        matplotlib.pyplot.savefig(arg[0] + 'a.png')
        matplotlib.pyplot.close()


    else:
        [print(erase_tup) for erase_tup in erase_list]
        print(count*mlc_size)
        print("bwrite(K) = ", bwrite/2)
        #matplotlib.pyplot.scatter(pblock, num_erase, marker = '_')

        
    
if __name__ == "__main__":
    main([sys.argv[1], sys.argv[2]], 1024)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
