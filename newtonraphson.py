import sys
import math

file = open('input')
n = int(file.readline())

def polyvalAt(x):
    return a*x*x*x + b*x*x + c*x + d
def dbydx_polyvalAt(x):
    return 3*a*x*x + 2*b*x + c

for line in file:
    if n == 0:
        break
    n = n - 1
    a, b, c, d, k = map(float, line.split())
    # transform F(t) = K to F(t) - K = 0
    d = d - k
    mid = 0
    while (1):
        if abs(polyvalAt(mid)) > 0.000001:
            mid = mid - polyvalAt(mid)/dbydx_polyvalAt(mid)
        else:
            break
    d = d + k
    print(int(mid))


